import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MatchpageComponent } from './matchpage/matchpage.component';
import { StatistiquepageComponent } from './statistiquepage/statistiquepage.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MatchComponent } from './match/match.component';
import {Zinderservice} from './service/zinderservice';
import {HttpClientModule} from '@angular/common/http';
import { NewprofilComponent } from './newprofil/newprofil.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MatchpageComponent,
    StatistiquepageComponent,
    HeaderComponent,
    FooterComponent,
    MatchComponent,
    NewprofilComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [Zinderservice
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
