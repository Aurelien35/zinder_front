import { Component, OnInit } from '@angular/core';
import {Zinderservice} from '../service/zinderservice';

@Component({
  selector: 'app-newprofil',
  templateUrl: './newprofil.component.html',
  styleUrls: ['./newprofil.component.css']
})
export class NewprofilComponent implements OnInit {

  constructor(private zinderservice: Zinderservice) { }

  submitProfile(name: string, prenom: string, url: string, interet1: string, interet2: string, interet3: string){
    this.zinderservice.postProfile().subscribe(result => {}}


  ngOnInit() {
  }

}
