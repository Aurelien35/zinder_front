import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewprofilComponent } from './newprofil.component';

describe('NewprofilComponent', () => {
  let component: NewprofilComponent;
  let fixture: ComponentFixture<NewprofilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewprofilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewprofilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
