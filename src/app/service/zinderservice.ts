import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Profil} from '../model/profil';
import {ListProfil} from '../model/ListProfil';
import {map} from 'rxjs/operators';

@Injectable()
export class Zinderservice {
  constructor(
    private http: HttpClient
  ) {
  }

  getProfils(): Observable<Profil[]> {
    return this.http
      .get<ListProfil>('http://localhost:8088/zinder/profils').pipe(
        map(listProfil => ListProfil));
  }

  postProfil(nom: string, prenom: string, url: string, interet1: string, interet2: string, interet3: string): Observable<NewProfilRepresentation> {
    return this.http
      .post<NewProfilRepresentation>('http://localhost:8088/zinder/profils', {nom, prenom, url, interet1, interet2, interet3});
  }
*/
}
