import {Component, Input, OnInit} from '@angular/core';
import {Zinderservice} from '../service/zinderservice';
import {Profil} from '../model/profil';
import {ListProfil} from '../model/ListProfil';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  constructor(private zinderservice: Zinderservice) {}

  @Input () id: number;

  @Input () nom: string;

  @Input () prenom: string;

  @Input () photoUrl: string;

  @Input () interets: string[];




  ngOnInit() {

  }
}
