export class NewProfilRepresentation {
  nom: string;
  prenom: string;
  url: string;
  interet1: string;
  interet2: string;
  interet3: string;


  constructor(nom: string, prenom: string, url: string, interet1: string, interet2: string, interet3: string) {
    this.nom = nom;
    this.prenom = prenom;
    this.url = url;
    this.interet1 = interet1;
    this.interet2 = interet2;
    this.interet3 = interet3;

  }
}
