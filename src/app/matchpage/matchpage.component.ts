import {Component, Input, OnInit} from '@angular/core';
import {Profil} from '../model/profil';
import {Zinderservice} from '../service/zinderservice';

@Component({
  selector: 'app-matchpage',
  templateUrl: './matchpage.component.html',
  styleUrls: ['./matchpage.component.css']
})
export class MatchpageComponent implements OnInit {

  listProfil: Profil[] = [];


  @Input () nom: string;

  constructor(private zinderservice: Zinderservice) {
  }
  ngOnInit() {
    this.zinderservice.getProfils().subscribe(List => {
      this.listProfil = List;
      console.log(List);
    });
  }
}
