import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatistiquepageComponent } from './statistiquepage.component';

describe('StatistiquepageComponent', () => {
  let component: StatistiquepageComponent;
  let fixture: ComponentFixture<StatistiquepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatistiquepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatistiquepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
